import { AngulardynamicformPage } from './app.po';

describe('angulardynamicform App', () => {
  let page: AngulardynamicformPage;

  beforeEach(() => {
    page = new AngulardynamicformPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Test');
  });
});
