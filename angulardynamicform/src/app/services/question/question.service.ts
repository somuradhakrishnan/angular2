import { Injectable } from '@angular/core';
import {QuestionBase} from "../../fields/question-base";
import {DropdownQuestion} from "../../fields/question-dropdown";
import {TextboxQuestion} from "../../fields/question-textbox";

@Injectable()
export class QuestionService {

  constructor() { }

  getQuestions() {

    let questions: QuestionBase<any>[] = [

      new DropdownQuestion({
        key: 'brave',
        label: 'Bravery Rating',
        options: [
          {key: 'solid',  value: 'Solid'},
          {key: 'great',  value: 'Great'},
          {key: 'good',   value: 'Good'},
          {key: 'unproven', value: 'Unproven'}
          ],
        order: 3

      }),
      new TextboxQuestion({
        key: 'firstName',
        label: 'First name',
        value: 'Bombasto',
        required: true,
        order: 1
      }),
      new TextboxQuestion({
        key: 'emailAddress',
        label: 'Email',
        type: 'email',
        order: 2
      })
    ];

    return questions.sort((a, b) => a.order - b.order);
  }

}
