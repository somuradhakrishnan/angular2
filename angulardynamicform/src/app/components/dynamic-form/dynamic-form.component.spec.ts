import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DynamicFormComponent } from './dynamic-form.component';
import {ReactiveFormsModule, FormGroup} from '@angular/forms';
import {DynamicFormQuestionComponent} from "../dynamic-form-question/dynamic-form-question.component";

describe('DynamicFormComponentComponent', () => {
  let component: DynamicFormComponent;
  let fixture: ComponentFixture<DynamicFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicFormComponent, DynamicFormQuestionComponent],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create a `FormGroup` comprised of `FormControl`s', () => {
    component.ngOnInit();
    expect(component.form instanceof FormGroup).toBe(true);
  });


  it('should create a `FormControl` for each question', () => {
    component.questions = [
      {
        key: 'firstName',
        controlType : 'textbox',
        label: 'First name',
        value: 'Bombasto',
        required: true,
        order: 1
      },
      {
        key: 'emailAddress',
        label: 'Email',
        value: 'dummy@gmail.com',
        controlType : 'textbox',
        required: true,
        order: 2
      }
    ];
    component.ngOnInit();
    expect(Object.keys(component.form.controls)).toEqual([
      'firstName', 'emailAddress'
    ]);
  });
});
