import {Component, OnInit, Input} from '@angular/core';
import {QuestionControlService} from "../../services/question-control/question-control.service";
import {QuestionBase} from "../../fields/question-base";
import {FormGroup} from "@angular/forms";

@Component({
  moduleId: module.id,
  selector: 'app-dynamic-form-component',
  templateUrl: 'dynamic-form.component.html',
  styleUrls: ['dynamic-form.component.css'],
  providers: [ QuestionControlService ]
})
export class DynamicFormComponent implements OnInit {

  @Input() questions: QuestionBase<any>[] = [];
  form: FormGroup;
  payLoad = '';

  constructor(private qcs: QuestionControlService) { }

  ngOnInit() {
    this.form = this.qcs.toFormGroup(this.questions);
  }

  onSubmit() {
    this.payLoad = JSON.stringify(this.form.value);
  }
}
