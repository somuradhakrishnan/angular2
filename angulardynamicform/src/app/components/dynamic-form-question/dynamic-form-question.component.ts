import {Component, Input} from '@angular/core';
import {QuestionBase} from "../../fields/question-base";
import {FormGroup} from "@angular/forms";

@Component({
  moduleId: module.id,
  selector: 'app-dynamic-form-question-component',
  templateUrl: 'dynamic-form-question.component.html',
  styleUrls: ['dynamic-form-question.component.css']
})
export class DynamicFormQuestionComponent {

  @Input() question: QuestionBase<any>;
  @Input() form: FormGroup;
  get isValid() { return this.form.controls[this.question.key].valid; }

}
