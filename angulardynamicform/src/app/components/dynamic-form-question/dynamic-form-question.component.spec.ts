import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DynamicFormQuestionComponent } from './dynamic-form-question.component';
import {FormsModule, ReactiveFormsModule, FormControl, FormGroup} from "@angular/forms";
import {QuestionControlService} from "../../services/question-control/question-control.service";

describe('DynamicFormQuestionComponentComponent', () => {
  let component: DynamicFormQuestionComponent;
  let fixture: ComponentFixture<DynamicFormQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicFormQuestionComponent ],
      imports:[FormsModule,ReactiveFormsModule],
      providers: [ QuestionControlService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    component.question =
      {
        key: 'firstName',
        controlType : 'textbox',
        label: 'First name',
        value: 'Bombasto',
        required: true,
        order: 1
      };
    component.isValid;
    expect(component).toBeTruthy();
  });*/
});
