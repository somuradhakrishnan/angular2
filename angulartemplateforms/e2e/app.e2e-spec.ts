import { AngulartemplateformsPage } from './app.po';

describe('angulartemplateforms App', function() {
  let page: AngulartemplateformsPage;

  beforeEach(() => {
    page = new AngulartemplateformsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
