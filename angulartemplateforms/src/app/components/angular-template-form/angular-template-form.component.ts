import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-angular-template-form',
  templateUrl: 'angular-template-form.component.html',
  styleUrls: ['angular-template-form.component.css']
})
export class AngularTemplateFormComponent{

  user: Object = {};

  onSubmitTemplateBased() {
    console.log("Test");
  }

}
