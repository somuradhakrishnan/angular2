/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularTemplateFormComponent } from './angular-template-form.component';
import { FormsModule } from '@angular/forms';

describe('AngularTemplateFormComponent', () => {
  let component: AngularTemplateFormComponent;
  let fixture: ComponentFixture<AngularTemplateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports : [FormsModule],
      declarations: [ AngularTemplateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularTemplateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
