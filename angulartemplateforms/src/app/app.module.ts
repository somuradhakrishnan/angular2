import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AngularTemplateFormComponent } from './components/angular-template-form/angular-template-form.component';

@NgModule({
  declarations: [
    AppComponent,
    AngularTemplateFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AngularTemplateFormComponent]
})
export class AppModule { }
